﻿using System;
namespace webapicore.Constant
{
    public class Config
    {
        public class ConfigDatabase
        {
            public const String CONFIG_DB = "mongodb://localhost:27017";
            public const String DB_NAME = "herb_data";
        }

        public class ConfigError
        {
            public const String TITEL = "แจ้งเตือน";
            public const String MESSAGE_SUCCESS = "SUCCESS";
            public const String MESSAGE_ERROR = "ERROR";
        }

        public class ConfigCollection
        {
            public const String C_HERB = "herb";
            public const String C_USER = "user";
        }
    }
}
