﻿using System;
using MongoDB.Driver;
using static webapicore.Constant.Config;

namespace webapicore.Utility.ConnectDB
{
    public class Connect
    {
        public static IMongoDatabase ConnectMongoClient()
        {
            MongoClient client = new MongoClient(ConfigDatabase.CONFIG_DB);
            var database = client.GetDatabase(ConfigDatabase.DB_NAME);
            return database;
        }
    }
}
