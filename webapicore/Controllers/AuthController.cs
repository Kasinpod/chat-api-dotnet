﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using webapicore.Model;
using webapicore.Repository.Interface;
using webapicore.Utility.Jwt;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace webapicore.Controllers
{
    [Route("api/auth")]
    public class AuthController : Controller
    {
        private readonly IAuthRepository _AuthRepository;

        public AuthController(IAuthRepository AuthRepository)
        {
            _AuthRepository = AuthRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]UserModel user)
        {
            var usr = await _AuthRepository.GetUser(user.Login, user.Password);

            if (usr != null)
            {
                var token = new JwtTokenBuilder()
                                    .AddSecurityKey(JwtSecurityKey.Create("key-value-token-expires"))
                                    .AddSubject(user.Login)
                                    .AddIssuer("issuerTest")
                                    .AddAudience("bearerTest")
                                    .AddClaim("MemberId", "111")
                                    .AddExpiry(1)
                                    .Build();

                return Ok(token.Value);

            }
            else
                return Unauthorized();
        }
    }
}
