﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using webapicore.Model;
using webapicore.Repository.Interface;
using webapicore.Utility.ConnectDB;
using static webapicore.Constant.Config;

namespace webapicore.Repository
{
    public class AuthRepository : IAuthRepository
    {
        private readonly IMongoCollection<UserModel> _collection;

        public AuthRepository()
        {
            var database = Connect.ConnectMongoClient();
            _collection = database.GetCollection<UserModel>(ConfigCollection.C_USER);
        }

        public Task<List<UserModel>> GetUsers()
        {
            var builder = Builders<UserModel>.Filter;

            return _collection.Find(_ => true).ToListAsync();
        }

        public Task<UserModel> GetUser(string login, string password)
        {
            var builder = Builders<UserModel>.Filter;
            var filter = builder.Eq("Login", login) & builder.Eq("Password", password);
            return _collection.Find(filter).FirstOrDefaultAsync();
        }

    }
}
