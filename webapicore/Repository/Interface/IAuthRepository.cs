﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using webapicore.Model;

namespace webapicore.Repository.Interface
{
    public interface IAuthRepository
    {
        Task<List<UserModel>> GetUsers();

        Task<UserModel> GetUser(string login, string password);
    }
}
